package `in`.zeta.apollo.joiningcomponents.sdk3.di

import `in`.zeta.apollo.joiningcomponents.di.CoreComponent
import `in`.zeta.apollo.joiningcomponents.sdk2.di.CipherSdkComponent
import `in`.zeta.apollo.joiningcomponents.sdk3.Swipe2PayActivity
import dagger.Component

@Swipe2PayScope
@Component(dependencies = [CoreComponent::class, CipherSdkComponent::class], modules = [Swipe2PayModule::class])
interface Swipe2PayComponent {

    fun inject(activity: Swipe2PayActivity)

}