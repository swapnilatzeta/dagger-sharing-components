package `in`.zeta.apollo.joiningcomponents.authmanager

import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random

@Singleton
class AuthManagerImpl @Inject constructor(): AuthManager {

    private val userId = UUID.randomUUID().toString()

    override fun getUserId(): String {
        return userId
    }
}