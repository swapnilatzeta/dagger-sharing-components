package `in`.zeta.apollo.joiningcomponents.sdk2.di

import javax.inject.Scope


@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class CipherSdkScope