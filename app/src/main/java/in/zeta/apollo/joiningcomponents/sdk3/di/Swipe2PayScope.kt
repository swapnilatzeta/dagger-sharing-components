package `in`.zeta.apollo.joiningcomponents.sdk3.di

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class Swipe2PayScope