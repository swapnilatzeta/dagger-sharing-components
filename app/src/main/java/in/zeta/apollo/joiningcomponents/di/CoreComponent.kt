package `in`.zeta.apollo.joiningcomponents.di

import `in`.zeta.apollo.joiningcomponents.authmanager.AuthManager
import `in`.zeta.apollo.joiningcomponents.authmanager.AuthManagerModule
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AuthManagerModule::class])
interface CoreComponent {

    fun authManager(): AuthManager

    @Component.Factory
    interface ComponentFactory {
        fun create(@BindsInstance context: Context): CoreComponent
    }

}