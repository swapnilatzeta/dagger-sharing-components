package `in`.zeta.apollo.joiningcomponents

import `in`.zeta.apollo.joiningcomponents.sdk1.CardsActivity
import `in`.zeta.apollo.joiningcomponents.sdk1.CardsSdk
import `in`.zeta.apollo.joiningcomponents.sdk2.SuperPinActivity
import `in`.zeta.apollo.joiningcomponents.sdk3.Swipe2PayActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var cardsButton: Button
    private lateinit var superPinButton: Button
    private lateinit var swipe2PayButton: Button

    @Inject
    lateinit var cardsSdk: CardsSdk

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun initView() {

        cardsButton = findViewById(R.id.btGetCard)
        superPinButton = findViewById(R.id.btSuperPin)
        swipe2PayButton = findViewById(R.id.bts2p)

        cardsButton.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, CardsActivity::class.java)
            startActivity(intent)
        })

        superPinButton.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, SuperPinActivity::class.java)
            startActivity(intent)
        })


        swipe2PayButton.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, Swipe2PayActivity::class.java)
            startActivity(intent)
        })
    }
}
