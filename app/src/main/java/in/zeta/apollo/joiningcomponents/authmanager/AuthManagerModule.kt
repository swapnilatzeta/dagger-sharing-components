package `in`.zeta.apollo.joiningcomponents.authmanager

import dagger.Binds
import dagger.Module

@Module
abstract class AuthManagerModule {

    @Binds
    abstract fun provideAuthManager(impl: AuthManagerImpl): AuthManager

}