package `in`.zeta.apollo.joiningcomponents.sdk2.di

import `in`.zeta.apollo.joiningcomponents.sdk2.CipherSdk
import `in`.zeta.apollo.joiningcomponents.sdk2.CipherSdkImpl
import dagger.Binds
import dagger.Module

@Module
abstract class CipherSdkModule {

    @Binds
    abstract fun provideCipherSdk(impl: CipherSdkImpl): CipherSdk

}