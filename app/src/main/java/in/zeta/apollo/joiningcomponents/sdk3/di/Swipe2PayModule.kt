package `in`.zeta.apollo.joiningcomponents.sdk3.di

import `in`.zeta.apollo.joiningcomponents.sdk3.Swipe2PaySdk
import `in`.zeta.apollo.joiningcomponents.sdk3.Swipe2PaySdkImpl
import dagger.Binds
import dagger.Module

@Module
abstract class Swipe2PayModule {

    @Binds
    abstract fun provideSwipe2PaySdk(impl: Swipe2PaySdkImpl): Swipe2PaySdk
}