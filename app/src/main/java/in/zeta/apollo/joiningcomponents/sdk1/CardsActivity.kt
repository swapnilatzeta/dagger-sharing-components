package `in`.zeta.apollo.joiningcomponents.sdk1

import `in`.zeta.apollo.joiningcomponents.MyApplication
import `in`.zeta.apollo.joiningcomponents.R
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import javax.inject.Inject

class CardsActivity : AppCompatActivity() {

    lateinit var text: TextView

    @Inject
    lateinit var cardsSdk: CardsSdk

    override fun onCreate(savedInstanceState: Bundle?) {

        MyApplication.INSTANCE!!.cardsSdkComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cards)

        initView()
    }

    private fun initView() {

        text = findViewById(R.id.textView)

        text.text = cardsSdk.getCardNumber()

    }
}
