package `in`.zeta.apollo.joiningcomponents.sdk1

import `in`.zeta.apollo.joiningcomponents.authmanager.AuthManager
import `in`.zeta.apollo.joiningcomponents.sdk1.di.CardsSdkScope
import javax.inject.Inject

@CardsSdkScope
class CardsSdkImpl @Inject constructor(private val authManager: AuthManager): CardsSdk {

    override fun getCardNumber(): String {
        return "123456xxxxxxxxxx $$$ ${authManager.getUserId()}"
    }
}