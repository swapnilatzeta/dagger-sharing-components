package `in`.zeta.apollo.joiningcomponents.sdk3

import `in`.zeta.apollo.joiningcomponents.authmanager.AuthManager
import `in`.zeta.apollo.joiningcomponents.sdk2.CipherSdk
import `in`.zeta.apollo.joiningcomponents.sdk3.di.Swipe2PayScope
import javax.inject.Inject

@Swipe2PayScope
class Swipe2PaySdkImpl @Inject constructor(
    private val authManager: AuthManager,
    private val cipherSdk: CipherSdk
): Swipe2PaySdk {

    override fun swipe(): String {
        return  authManager.getUserId() + " swipe$$$ to pay " + cipherSdk.getSuperPin()
    }
}