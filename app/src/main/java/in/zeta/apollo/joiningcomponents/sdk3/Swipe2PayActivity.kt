package `in`.zeta.apollo.joiningcomponents.sdk3

import `in`.zeta.apollo.joiningcomponents.MyApplication
import `in`.zeta.apollo.joiningcomponents.R
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import javax.inject.Inject

class Swipe2PayActivity : AppCompatActivity() {

    lateinit var text: TextView

    @Inject
    lateinit var swipe2PaySdk: Swipe2PaySdk

    override fun onCreate(savedInstanceState: Bundle?) {

        MyApplication.INSTANCE!!.swipe2PayComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swipe2_pay)

        initView()
    }

    private fun initView() {

        text = findViewById(R.id.textView)

        text.text = swipe2PaySdk.swipe()

    }
}
