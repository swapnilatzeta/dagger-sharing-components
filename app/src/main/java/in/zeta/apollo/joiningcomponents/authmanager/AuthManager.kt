package `in`.zeta.apollo.joiningcomponents.authmanager

interface AuthManager {
    fun getUserId(): String
}