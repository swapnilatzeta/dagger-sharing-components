package `in`.zeta.apollo.joiningcomponents

import `in`.zeta.apollo.joiningcomponents.di.CoreComponent
import `in`.zeta.apollo.joiningcomponents.di.DaggerCoreComponent
import `in`.zeta.apollo.joiningcomponents.sdk1.di.CardsSdkComponent
import `in`.zeta.apollo.joiningcomponents.sdk1.di.DaggerCardsSdkComponent
import `in`.zeta.apollo.joiningcomponents.sdk2.di.CipherSdkComponent
import `in`.zeta.apollo.joiningcomponents.sdk2.di.DaggerCipherSdkComponent
import `in`.zeta.apollo.joiningcomponents.sdk3.di.DaggerSwipe2PayComponent
import `in`.zeta.apollo.joiningcomponents.sdk3.di.Swipe2PayComponent
import android.app.Application

class MyApplication: Application() {

    companion object {
        var INSTANCE: MyApplication? = null
    }

    private val coreComponent: CoreComponent by lazy {
        DaggerCoreComponent.factory().create(this)
    }

    val cardsSdkComponent: CardsSdkComponent by lazy {
        DaggerCardsSdkComponent.builder().coreComponent(coreComponent).build()
    }

    val cipherSdkComponent: CipherSdkComponent by lazy {
        DaggerCipherSdkComponent.builder().coreComponent(coreComponent).build()
    }

    val swipe2PayComponent: Swipe2PayComponent by lazy {
        DaggerSwipe2PayComponent.builder()
            .coreComponent(coreComponent)
            .cipherSdkComponent(cipherSdkComponent)
            .build()
    }


    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

}