package `in`.zeta.apollo.joiningcomponents.sdk1.di

import `in`.zeta.apollo.joiningcomponents.sdk1.CardsSdk
import `in`.zeta.apollo.joiningcomponents.sdk1.CardsSdkImpl
import dagger.Binds
import dagger.Module

@Module
abstract class CardsSdkModule {

    @Binds
    abstract fun provideCardsSdk(impl: CardsSdkImpl): CardsSdk

}