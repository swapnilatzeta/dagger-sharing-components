package `in`.zeta.apollo.joiningcomponents.sdk2.di

import `in`.zeta.apollo.joiningcomponents.di.CoreComponent
import `in`.zeta.apollo.joiningcomponents.sdk2.CipherSdk
import `in`.zeta.apollo.joiningcomponents.sdk2.SuperPinActivity
import dagger.Component

@CipherSdkScope
@Component(dependencies = [CoreComponent::class], modules = [CipherSdkModule::class])
interface CipherSdkComponent {

    fun cipherSkd(): CipherSdk

    fun inject(activity: SuperPinActivity)

    /*@Component.Factory
    interface ComponentFactory {
        fun create(@BindsInstance coreComponent: CoreComponent): CardsSdkComponent
    }*/

}