package `in`.zeta.apollo.joiningcomponents.sdk1.di

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class CardsSdkScope