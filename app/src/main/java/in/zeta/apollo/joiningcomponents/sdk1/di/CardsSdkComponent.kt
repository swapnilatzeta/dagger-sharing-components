package `in`.zeta.apollo.joiningcomponents.sdk1.di

import `in`.zeta.apollo.joiningcomponents.di.CoreComponent
import `in`.zeta.apollo.joiningcomponents.sdk1.CardsActivity
import dagger.Component

@CardsSdkScope
@Component(dependencies = [CoreComponent::class], modules = [CardsSdkModule::class])
interface CardsSdkComponent {

    //fun cardsSdk(): CardsSdk

    fun inject(activity: CardsActivity)

    /*@Component.Factory
    interface ComponentFactory {
        fun create(@BindsInstance coreComponent: CoreComponent): CardsSdkComponent
    }*/

}