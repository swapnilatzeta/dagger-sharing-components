package `in`.zeta.apollo.joiningcomponents.sdk2

import `in`.zeta.apollo.joiningcomponents.authmanager.AuthManager
import `in`.zeta.apollo.joiningcomponents.sdk2.di.CipherSdkScope
import javax.inject.Inject

@CipherSdkScope
class CipherSdkImpl @Inject constructor(private val authManager: AuthManager): CipherSdk {

    override fun getSuperPin(): String {
        return "5614 super pin $$$ ${authManager.getUserId()}"
    }
}