package `in`.zeta.apollo.joiningcomponents.sdk2

import `in`.zeta.apollo.joiningcomponents.MyApplication
import `in`.zeta.apollo.joiningcomponents.R
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import javax.inject.Inject

class SuperPinActivity : AppCompatActivity() {

    lateinit var text: TextView


    @Inject
    lateinit var cipherSdk: CipherSdk

    override fun onCreate(savedInstanceState: Bundle?) {

        MyApplication.INSTANCE!!.cipherSdkComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_super_pin)

        initView()
    }

    private fun initView() {

        text = findViewById(R.id.textView)

        text.text = cipherSdk.getSuperPin()

    }
}
